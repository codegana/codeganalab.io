<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="dad" />
        <title>Codegana</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">Codegana</a>
                <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#tecnologias">Tecnologías</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#acerca">acerca</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contacto">contacto</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- masthead-->
        <header class="masthead bg-primary text-white text-center">
            <div class="container d-flex align-items-center flex-column">
                <!-- masthead Avatar Image-->
                <img class="masthead-avatar mb-5" src="assets/img/code.png" alt="" />
                <!-- masthead Heading-->
                <h1 class="masthead-heading text-uppercase mb-0">Codegana</h1>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- masthead Subheading-->
                <p class="masthead-subheading font-weight-light mb-0">Clases de Programación en Cortegana</p>
            </div>
        </header>
        <!-- tecnologias Section-->
        <section class="page-section tecnologias" id="tecnologias">
            <div class="container">
                <!-- tecnologias Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Tecnologías</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- tecnologias Grid Items-->
                <div class="row">
                    <!-- tecnologias Item 1-->
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="tecnologias-item mx-auto" data-toggle="modal" data-target="#tecnologiasModal1">
                            <div class="tecnologias-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="tecnologias-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/tecnologias/microbit.png" alt="" />
                        </div>
                    </div>
                    <!-- tecnologias Item 2-->
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="tecnologias-item mx-auto" data-toggle="modal" data-target="#tecnologiasModal2">
                            <div class="tecnologias-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="tecnologias-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/tecnologias/makecode.png" alt="" />
                        </div>
                    </div>
                    <!-- tecnologias Item 3-->
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="tecnologias-item mx-auto" data-toggle="modal" data-target="#tecnologiasModal3">
                            <div class="tecnologias-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="tecnologias-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/tecnologias/scratch.png" alt="" />
                        </div>
                    </div>
                    <!-- tecnologias Item 4-->
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
                        <div class="tecnologias-item mx-auto" data-toggle="modal" data-target="#tecnologiasModal4">
                            <div class="tecnologias-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="tecnologias-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/tecnologias/python.png" alt="" />
                        </div>
                    </div>
                    <!-- tecnologias Item 5-->
                    <div class="col-md-6 col-lg-4 mb-5 mb-md-0">
                        <div class="tecnologias-item mx-auto" data-toggle="modal" data-target="#tecnologiasModal5">
                            <div class="tecnologias-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="tecnologias-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/tecnologias/raspberrypi.png" alt="" />
                        </div>
                    </div>
                    <!-- tecnologias Item 6-->
                    <div class="col-md-6 col-lg-4">
                        <div class="tecnologias-item mx-auto" data-toggle="modal" data-target="#tecnologiasModal6">
                            <div class="tecnologias-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="tecnologias-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/tecnologias/arduino.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- acerca Section-->
        <section class="page-section bg-primary text-white mb-0" id="acerca">
            <div class="container">
                <!-- acerca Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-white">acerca</h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- acerca Section Content-->
                <div class="row">
                    <div class="col-lg-4 ml-auto"><p class="lead">Ofrezco clases de programación a domicilio en Cortegana. La clases se adaptan a los intereses de los alumnos. En el apartado de tecnología de esta pagina he puesto algún tema principal, que sigue un patrón evolutivo, desde lenguaje en bloques hasta lenguaje en código, pasando para adaptación a los microcontroladores y placas SBC. Esto es solo un posible plan de desarrollo, el mas común, que no impide buscar otra sendas, como la programación de videojuegos o la robótica por ejemplo. Trabajo principalmente con edades comprendidas entres 8 y 15 anos, aunque hacer una excepción para edades mas avanzadas no sería ningún problema.</p></div>
                    <div class="col-lg-4 mr-auto"><p class="lead">Me llamo Davide, he estudiado informática en la Universidad de Pisa. Mi interés para lo ordenadores y la electrónica me ha llevado a fundir mis conocimientos en lo que últimamente se llama <a style="color:orange;text-decoration:none" target=_blank href="https://medium.com/@danielitohead/la-cultura-maker-y-por-qu%C3%A9-nos-interesa-tanto-desde-la-educaci%C3%B3n-f7c6b1703fd4">Cultura Maker</a>. Llevo viviendo en la Sierra 8 años, junto a mi familia, tiempo en que he seguido formándome con y para mis hijos. He trabajado en la empresa <a style="color:orange;text-decoration:none" target=_blank href="https://www.mindlabs.es/">Mindlabs</a> de Huelva, impartiendo cursos de robótica educativa en Aracena y diseñando actividades <a style="color:orange;text-decoration:none" target=_blank "http://innovacion.uas.edu.mx/educacion-steam-science-technology-engineering-arts-and-math/">S.T.E.A.M</a> para la Asociacion de Altas Capacidades ARETÉ. Si quiere probar una sesión gratuitamente o hablar conmigo, puedes contactarme por movil o whatsapp.</p></div>
                </div> 
            </div>
        </section>
        <!-- contacto Section-->
        <section class="page-section" id="contacto">
            <div class="container">
                <!-- contacto Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">contacto</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- contacto Section Form-->
                <div class="row center">
                  <button type="button" class="btn btn-primary btn-lg btn-block"><a href="tel:606110139" style="text-decoration: none; color: white;">Movil y Whatsapp: 606 11 01 39</a></button>
                </div>
              </div>
            </div>
        </section>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Codegana 2020</small></div>
        </div>
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- tecnologias Modals-->
        <!-- tecnologias Modal 1-->
        <div class="tecnologias-modal modal fade" id="tecnologiasModal1" tabindex="-1" role="dialog" aria-labelledby="tecnologiasModal1Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- tecnologias Modal - Title-->
                                    <h2 class="tecnologias-modal-title text-secondary text-uppercase mb-0" id="tecnologiasModal1Label">Microbit</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- tecnologias Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/tecnologias/microbit.png" alt="" />
                                    <!-- tecnologias Modal - Text-->
                                    <p class="mb-5"><a target=_blank href="http://microes.org/">Micro:bit</a> es una pequeña tarjeta programable de 4x5 cm diseñada para que aprender a programar sea fácil, divertido y al alcance de todos. Tiene un entorno de programación gráfico propio: MakeCode de Microsoft, un sencillo editor gráfico online muy potente. También se puede programar con JavaScript, Pyton y Scratch </p>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Cerrar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- tecnologias Modal 2-->
        <div class="tecnologias-modal modal fade" id="tecnologiasModal2" tabindex="-1" role="dialog" aria-labelledby="tecnologiasModal2Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- tecnologias Modal - Title-->
                                    <h2 class="tecnologias-modal-title text-secondary text-uppercase mb-0" id="tecnologiasModal2Label">Makecode</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- tecnologias Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/tecnologias/makecode.png" alt="" />
                                    <!-- tecnologias Modal - Text-->
                                    <p class="mb-5"><a target=_blank href="https://www.microsoft.com/es-es/makecode">Microsoft Makecode</a> acerca la informática a todos los alumnos con proyectos divertidos, resultados inmediatos y editores de bloques y de texto para estudiantes de distintos niveles.</p>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Cerrar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- tecnologias Modal 3-->
        <div class="tecnologias-modal modal fade" id="tecnologiasModal3" tabindex="-1" role="dialog" aria-labelledby="tecnologiasModal3Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- tecnologias Modal - Title-->
                                    <h2 class="tecnologias-modal-title text-secondary text-uppercase mb-0" id="tecnologiasModal3Label">Scratch</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- tecnologias Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/tecnologias/scratch.png" alt="" />
                                    <!-- tecnologias Modal - Text-->
                                    <p class="mb-5"><a target=_blank href="https://scratch.mit.edu/">Scratch</a> es un lenguaje de programación visual desarrollado por el Grupo Lifelong Kindergarten del MIT Media Lab. Su principal característica consiste en que permite el desarrollo de habilidades mentales mediante el aprendizaje de la programación sin tener conocimientos profundos sobre el código. Sus características ligadas al fácil entendimiento del pensamiento computacional han hecho que sea muy difundido en la educación de niños, adolescentes y adultos. </p>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Cerrar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- tecnologias Modal 4-->
        <div class="tecnologias-modal modal fade" id="tecnologiasModal4" tabindex="-1" role="dialog" aria-labelledby="tecnologiasModal4Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- tecnologias Modal - Title-->
                                    <h2 class="tecnologias-modal-title text-secondary text-uppercase mb-0" id="tecnologiasModal4Label">Python</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- tecnologias Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/tecnologias/python.png" alt="" />
                                    <!-- tecnologias Modal - Text-->
                                    <p class="mb-5"><a target=_blank href="https://www.python.org/">Python</a> es la navaja suiza de los programadores. Se trata de un veterano lenguaje de programación presente en multitud de aplicaciones y sistemas operativos. Podemos encontrarlo corriendo en servidores, en aplicaciones iOS, Android, Linux, Windows o Mac. Esto es debido a que cuenta con una curva de aprendizaje moderada ya que su filosofía hace hincapié en ofrecer una sintaxis de código legible.</p>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Cerrar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- tecnologias Modal 5-->
        <div class="tecnologias-modal modal fade" id="tecnologiasModal5" tabindex="-1" role="dialog" aria-labelledby="tecnologiasModal5Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- tecnologias Modal - Title-->
                                    <h2 class="tecnologias-modal-title text-secondary text-uppercase mb-0" id="tecnologiasModal5Label">Raspberry Pi</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- tecnologias Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/tecnologias/raspberrypi.png" alt="" />
                                    <!-- tecnologias Modal - Text-->
                                    <p class="mb-5">Las <a target=_blank href="https://www.raspberrypi.org/">Raspberry Pi</a> es una serie de ordenadores de placa reducida, ordenadores de placa única u ordenadores de placa simple (SBC) de bajo costo desarrollado en el Reino Unido por la Raspberry Pi Foundation, con el objetivo de poner en manos de las personas de todo el mundo el poder de la informática y la creación digital. Si bien el modelo original buscaba la promoción de la enseñanza de informática en las escuelas, este acabó siendo más popular de lo que se esperaba, hasta incluso vendiéndose fuera del mercado objetivo para usos como robótica. </p>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Cerrar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- tecnologias Modal 6-->
        <div class="tecnologias-modal modal fade" id="tecnologiasModal6" tabindex="-1" role="dialog" aria-labelledby="tecnologiasModal6Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- tecnologias Modal - Title-->
                                    <h2 class="tecnologias-modal-title text-secondary text-uppercase mb-0" id="tecnologiasModal6Label">Arduino</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- tecnologias Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/tecnologias/arduino.png" alt="" />
                                    <!-- tecnologias Modal - Text-->
                                    <p class="mb-5"><a target=_blank href="https://www.arduino.cc/">Arduino</a> es una plataforma de creación de electrónica de código abierto, la cual está basada en hardware y software libre, flexible y fácil de utilizar para los creadores y desarrolladores. Esta plataforma permite crear diferentes tipos de microordenadores de una sola placa a los que la comunidad de creadores puede darles diferentes tipos de uso.</p>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Cerrar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
